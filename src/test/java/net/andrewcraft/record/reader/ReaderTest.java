package net.andrewcraft.record.reader;

import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;

/**
 * Base class for {@link RecordReader} tests.
 */
public class ReaderTest {

    /**
     * Gets a {@link File} object for a file in the class path.
     *
     * @param file file to get
     * @return {@link File} object.
     */
    public File getRecordsFile(String file) {
        if (!file.startsWith("/")) {
            file = "/" + file;
        }

        try {
            URL url = this.getClass().getResource(file);
            return new File(url.toURI());
        } catch (URISyntaxException e) {
            throw new IllegalArgumentException("Failed to load file: '" + file + "', error: " + e.getMessage(), e);
        }
    }

}
