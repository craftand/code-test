package net.andrewcraft.record.reader;

import net.andrewcraft.record.Record;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.util.Set;

import static org.junit.Assert.*;


/**
 * Tests for {@link XmlRecordReader}.
 */
public class XmlRecordReaderTest extends ReaderTest {

    private RecordReader reader;
    private File recordsFile;

    @Before
    public void setUp() throws Exception {
        reader = new XmlRecordReader();
    }

    @After
    public void tearDown() throws Exception {
        reader = null;
        recordsFile = null;
    }

    @Test
    public void getRecords() throws Exception {
        recordsFile = getRecordsFile("test.xml");
        Set<Record> records = reader.getRecords(recordsFile);

        assertNotNull(records);
        assertEquals(4, records.size());


    }

    @Test
    public void accepts() throws Exception {

        assertTrue(reader.accepts(new File("test.xml")));
        assertFalse(reader.accepts(new File("test.txt")));

    }

}