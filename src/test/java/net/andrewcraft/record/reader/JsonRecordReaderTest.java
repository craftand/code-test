package net.andrewcraft.record.reader;

import net.andrewcraft.record.Record;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.net.URISyntaxException;
import java.util.Set;

import static org.junit.Assert.*;

/**
 * Tests for {@link JsonRecordReader}.
 */
public class JsonRecordReaderTest extends ReaderTest {

    private RecordReader reader;
    private File recordsFile;

    @Before
    public void before() throws URISyntaxException {
        reader = new JsonRecordReader();

    }

    @After
    public void after() {
        reader = null;
        recordsFile = null;
    }

    @Test
    public void testLoadFile() {
        recordsFile = getRecordsFile("test.json");
        Set<Record> records = reader.getRecords(recordsFile);

        assertNotNull(records);
        assertTrue(records.size() > 0);
        assertEquals(3, records.size());
    }

    @Test
    public void testLoadInvalidFile() {
        recordsFile = getRecordsFile("test-invalid.json");
        Set<Record> records = reader.getRecords(recordsFile);

        assertNull(records);
    }

    @Test
    public void testBadDate() {
        recordsFile = getRecordsFile("test-bad-date.json");
        Set<Record> records = reader.getRecords(recordsFile);

        assertNotNull(records);
        assertEquals(2, records.size());
    }

    @Test
    public void testBadArray() {
        recordsFile = getRecordsFile("test-bad-array.json");
        Set<Record> records = reader.getRecords(recordsFile);

        assertNull(records);
    }

    @Test
    public void testAccespts() {
        assertTrue(reader.accepts(new File("test.json")));
        assertFalse(reader.accepts(new File("test.txt")));
    }
}