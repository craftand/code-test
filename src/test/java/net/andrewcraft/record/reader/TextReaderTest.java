package net.andrewcraft.record.reader;

import net.andrewcraft.record.Record;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.util.Set;

import static org.junit.Assert.*;

/**
 * Tests for {@link TextRecordReader}.
 */
public class TextReaderTest extends ReaderTest {

    private RecordReader reader;
    private File recordsFile;

    @Before
    public void setUp() throws Exception {
        reader = new TextRecordReader();
    }

    @After
    public void tearDown() throws Exception {
        reader = null;
        recordsFile = null;
    }

    @Test
    public void getRecordsTabs() throws Exception {
        recordsFile = getRecordsFile("test.txt");
        Set<Record> records = reader.getRecords(recordsFile);

        assertNotNull(records);
        assertEquals(5, records.size());

    }

    @Test
    public void getRecordsCsv() throws Exception {
        recordsFile = getRecordsFile("test2.txt");
        Set<Record> records = reader.getRecords(recordsFile);

        assertNotNull(records);
        assertEquals(4, records.size());
    }

    @Test
    public void accepts() throws Exception {

        assertTrue(reader.accepts(new File("test.txt")));
        assertFalse(reader.accepts(new File("test.json")));

    }

}