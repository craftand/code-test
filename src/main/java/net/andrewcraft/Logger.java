package net.andrewcraft;

/**
 * Simple logger interface with a default log method.
 */
public interface Logger {

    /**
     * Log a message to the console.
     *
     * @param logMessage the message to log.
     */
    default void log(String logMessage) {
        System.out.println(logMessage);
    }
}
