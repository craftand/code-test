package net.andrewcraft.record;

/**
 * Data object for a Record in the data file.
 */
public class Record {
    private final String firstname;
    private final String lastname;
    private final int age;
    private final double balance;

    public Record(String firstname, String lastname, int age, double balance) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.age = age;
        this.balance = balance;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public int getAge() {
        return age;
    }

    public double getBalance() {
        return balance;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Record record = (Record) o;

        if (age != record.age) return false;
        if (Double.compare(record.balance, balance) != 0) return false;
        if (!firstname.equals(record.firstname)) return false;
        return lastname.equals(record.lastname);

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = firstname.hashCode();
        result = 31 * result + lastname.hashCode();
        result = 31 * result + age;
        temp = Double.doubleToLongBits(balance);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public String toString() {
        return "Record{" +
                "age='" + age + '\'' +
                ", lastname='" + lastname + '\'' +
                ", firstname=" + firstname +
                ", balance=" + balance +
                '}';
    }
}
