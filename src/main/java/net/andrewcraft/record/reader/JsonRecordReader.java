package net.andrewcraft.record.reader;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Sets;
import net.andrewcraft.Logger;
import net.andrewcraft.record.Record;

import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Set;

import static com.google.common.io.Files.getFileExtension;

/**
 * RecordReader implementation for json files.
 * <p>
 * Expects a json array of objects with birthday, lastname, firstname and balance properties.
 * <p>
 * Expected birthday format is 'd-MMMM-yyyy'.
 */
public class JsonRecordReader implements RecordReader, Logger {

    private static final String DATE_FORMAT = "d-MMMM-yyyy";
    private final DateTimeFormatter formatter;
    private final ObjectMapper mapper;
    private final LocalDate now;

    public JsonRecordReader() {

        this.mapper = new ObjectMapper();
        this.formatter = DateTimeFormatter.ofPattern(DATE_FORMAT);
        this.now = LocalDate.now();
    }

    public Set<Record> getRecords(File recordsFile) {
        Set<Record> records = Sets.newHashSet();
        try {
            JsonNode nodes = mapper.readTree(recordsFile);

            if (!nodes.isArray()) {
                log("Expected first json node to be of array type.");
                return null;
            }

            for (JsonNode node : nodes) {
                Record r = parseRecord(node);
                if (null != r) {
                    records.add(r);
                }
            }
        } catch (IOException e) {
            log("Failed to read json file: " + recordsFile.getName() + ", error: " + e.getMessage());
            return null;
        }

        return records;
    }

    @Override
    public boolean accepts(File recordsFile) {
        return getFileExtension(recordsFile.getName()).toLowerCase().equals("json");
    }

    Record parseRecord(JsonNode node) {

        String firstname = node.path("firstname").asText();
        String lastname = node.path("lastname").asText();
        double balance = node.path("balance").asDouble();
        LocalDate birthday = null;
        try {
            birthday = LocalDate.parse(node.path("birthday").asText(), formatter);
        } catch (DateTimeParseException pe) {
            log("Invalid date format found, error: " + pe.getMessage());
            return null;
        }

        Period agePeriod = Period.between(birthday, now);
        int age = agePeriod.getYears();

        return new Record(firstname, lastname, age, balance);
    }
}
