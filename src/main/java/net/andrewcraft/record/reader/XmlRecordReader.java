package net.andrewcraft.record.reader;

import com.google.common.collect.Sets;
import net.andrewcraft.Logger;
import net.andrewcraft.record.Record;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.Set;

import static com.google.common.io.Files.getFileExtension;

/**
 * RecordReader implementation for xml files.
 * <p>
 * Expects to find data in person elements with age, lastname, firstname and balance elements.
 */
public class XmlRecordReader implements RecordReader, Logger {
    private final DocumentBuilder builder;

    public XmlRecordReader() {
        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        try {
            builder = docFactory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            throw new RuntimeException("Failed to create document builder, error: " + e.getMessage());
        }

    }

    @Override
    public Set<Record> getRecords(File recordsFile) {
        Set<Record> records = Sets.newHashSet();
        Document doc;
        try {
            doc = builder.parse(recordsFile);
            doc.getDocumentElement().normalize();
            NodeList nodeList = doc.getElementsByTagName("person");
            int len = nodeList.getLength();

            for (int i = 0; i < len; i++) {
                Node node = nodeList.item(i);
                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element element = (Element) node;
                    String firstname = getElementValue(element, "firstname");
                    String lastname = getElementValue(element, "lastname");
                    int age = Integer.parseInt(getElementValue(element, "age"));
                    double balance = Double.parseDouble(getElementValue(element, "balance"));

                    records.add(new Record(firstname, lastname, age, balance));
                }
            }
        } catch (SAXException | IOException e) {
            log("Failed to load xml records file: '" + recordsFile.getName() + "', error: " + e.getMessage());
            return null;
        }

        return records;
    }

    @Override
    public boolean accepts(File recordsFile) {
        return getFileExtension(recordsFile.getName()).toLowerCase().equals("xml");
    }

    String getElementValue(Element element, String tag) {
        return element.getElementsByTagName(tag).item(0).getTextContent();
    }
}
