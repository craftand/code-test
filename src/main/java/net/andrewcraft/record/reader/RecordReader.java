package net.andrewcraft.record.reader;

import net.andrewcraft.record.Record;

import java.io.File;
import java.util.Set;

/**
 * RecordReader interface for files containing {@link Record} types.
 */
public interface RecordReader {

    /**
     * Reads all records from the given file.
     *
     * @param recordsFile the file containing the records.
     * @return the set of all records or null if there is data issues.
     */
    Set<Record> getRecords(final File recordsFile);

    /**
     * Check if a file can be accepted. Each implementation can determine what files it can process.
     *
     * @param recordsFile the file to check if we accept.
     * @return whether or not to we can process the file.
     */
    boolean accepts(final File recordsFile);

}
