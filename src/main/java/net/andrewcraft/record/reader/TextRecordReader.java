package net.andrewcraft.record.reader;

import com.google.common.base.CharMatcher;
import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import net.andrewcraft.Logger;
import net.andrewcraft.record.Record;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static com.google.common.io.Files.getFileExtension;

/**
 * RecordReader implementation for text files.
 * <p>
 * Expects fields to be either tab or comma separated and values surrounded in '"'
 */
public class TextRecordReader implements RecordReader, Logger {

    private final Splitter splitter;

    public TextRecordReader() {
        splitter = Splitter.on(Pattern.compile(",\"|\t\""))
                .omitEmptyStrings()
                .trimResults(CharMatcher.is('"'));
    }

    @Override
    public Set<Record> getRecords(File recordsFile) {
        Set<Record> records = Sets.newHashSet();
        final List<String> header = Lists.newArrayList();
        final List<List<String>> toParse = Lists.newArrayList();
        try {
            Files.lines(recordsFile.toPath()).forEach(line -> {
                List<String> split = splitter.splitToList(line);
                if (header.isEmpty()) {
                    header.addAll(split);
                } else {
                    toParse.add(split);
                }
            });
        } catch (IOException e) {
            log("Failed to read file: '" + recordsFile.getName() + "', error: " + e.getMessage());
            return null;
        }

        int ageIndex = header.indexOf("age");
        int balanceIndex = header.indexOf("balance");

        return toParse.stream().map(values -> {

            String firstname = values.get(0);
            String lastname = values.get(1);
            int age = Integer.parseInt(values.get(ageIndex).replace(",", ""));
            double balance = Double.parseDouble(values.get(balanceIndex).replace(",", ""));

            return new Record(firstname, lastname, age, balance);

        }).collect(Collectors.toSet());
    }

    @Override
    public boolean accepts(File recordsFile) {
        return getFileExtension(recordsFile.getName()).toLowerCase().equals("txt");
    }
}
