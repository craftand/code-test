package net.andrewcraft;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import net.andrewcraft.record.Record;
import net.andrewcraft.record.reader.JsonRecordReader;
import net.andrewcraft.record.reader.RecordReader;
import net.andrewcraft.record.reader.TextRecordReader;
import net.andrewcraft.record.reader.XmlRecordReader;

import java.io.File;
import java.text.DecimalFormat;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Entry Point for code test program.
 * <p>
 * Accepts the following command line params:
 * <ul>
 * <li>-d directory - The directory where there data files are. Default is current directory.</li>
 * <li>-o order - The sort order for the data. Default is ASC.</li>
 * </ul>
 */
public class App implements Logger {
    private static final List<String> FILES = ImmutableList.of("data1.txt", "data2.txt", "data.json", "data.xml");
    private static final String RECORD_LOG_FORMAT = "|%5s|%12s|%12s|%10s|";
    private final List<RecordReader> recordReaders;
    private final File dataFileDir;

    private Comparator<Record> ageComparator = (r1, r2) -> Integer.compare(r1.getAge(), r2.getAge());
    private Comparator<Record> lastnameComparator = (r1, r2) -> String.CASE_INSENSITIVE_ORDER.compare(r1.getLastname(), r2.getLastname());

    public static void main(String[] args) {
        String order = "asc";
        File dir = new File(".");
        if (args.length > 0) {
            List<String> argsList = Lists.newArrayList(args);
            int dirIndex = argsList.indexOf("-d");
            int orderIndex = argsList.indexOf("-o");

            if (-1 != dirIndex) {
                try {
                    dir = new File(argsList.get(dirIndex + 1));
                } catch (Exception e) {
                    System.out.println("Error failed to find value for '-d' argument.");
                    System.exit(1);
                }
                if (!dir.exists()) {
                    System.out.println("Error the directory supplied: '" + dir.getAbsolutePath() + "' does not exist.");
                    System.exit(2);
                }

                if (!dir.isDirectory()) {
                    System.out.println("Error the directory supplied: '" + dir.getAbsolutePath() + "' is not a directory.");
                    System.exit(3);
                }
            }

            if (-1 != orderIndex) {
                try {
                    String argOrder = argsList.get(orderIndex + 1).toLowerCase();
                    if (!argOrder.equals("asc") && !argOrder.equals("desc")) {
                        System.out.println("Invalid argument for -o argument, using default 'asc'");
                    } else {
                        order = argOrder;
                    }
                } catch (Exception e) {
                    System.out.println("Error failed to find value for '-o' argument.");
                    System.exit(4);
                }

            }
        }

        new App(order, dir);
    }

    public App(final String sortOrder, final File dir) {
        dataFileDir = dir;
        recordReaders = ImmutableList.of(
                new TextRecordReader(), new XmlRecordReader(), new JsonRecordReader());

        if ("desc".equals(sortOrder)) {
            ageComparator = ageComparator.reversed();
            lastnameComparator = lastnameComparator.reversed();
        }

        try {
            log("Data is being sorted in " + sortOrder.toUpperCase() + " order");
            List<Record> records = FILES.stream().
                    flatMap(this::fileToRecords).
                    sorted(ageComparator.thenComparing(lastnameComparator)).collect(Collectors.toList());

            printHeader();

            records.forEach(this::printRecord);
        } catch (Exception e) {
            log("Error an unexpected error has occurred, error: " + e.getMessage());
            System.exit(1);
        }
    }

    private void printRecord(Record record) {
        log(String.format(RECORD_LOG_FORMAT,
                record.getAge(), record.getLastname(), record.getFirstname(),
                new DecimalFormat().format(record.getBalance())));
    }

    private void printHeader() {
        String header = String.format(RECORD_LOG_FORMAT, "Age", "Last Name", "First Name", "Balance");
        log(header);
    }


    private Stream<Record> fileToRecords(String s) {
        File file = new File(dataFileDir, s);
        if (!file.exists()) {
            log("File: '" + s + "' is not readable.");
        }

        return recordReaders.stream().
                filter(r -> r.accepts(file)).
                findFirst().
                get().
                getRecords(file).stream();
    }
}
