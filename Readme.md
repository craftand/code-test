##Code Test README

### 1. Requirements
To build this project you will need to have Java 8 installed and an active internet connection.

### 2. Building
To build the project execute the maven wrapper script. For windows run `mvnw.cmd package`. For os/x, linux run `./mvnw package`. The jar will be located in the target directory as codetest.jar.

### 3. Running
To run the program use the command `java -jar target/codetest.jar`. By default results are sorted in ascending order by age then lastname. To use descending order add `-o desc`. The data files are by default read from the current directory. To choose a different directory add `-d directory` to the command line.
